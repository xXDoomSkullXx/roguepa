﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {

    public int pointsToAdd;
    public AudioSource coinAudio;


    void OnTriggerEnter2D (Collider2D other)
    {
        

            if (other.GetComponent<PlayerController>() == null)
            {
                return;
            }

            ScoreCounter.AddPoints(pointsToAdd);
            coinAudio.Play();
            gameObject.SetActive(false);

        
    }
}
