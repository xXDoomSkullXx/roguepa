﻿using UnityEngine;
using System.Collections;

public class HurtEnemy : MonoBehaviour {

    public int damageToGive;

    public GameObject enemy;

    private Animator animator;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D enemy)
    {
        enemy.GetComponent<KillEnemy>().giveDamage(damageToGive);
        animator.SetTrigger("playerAttack");
        
    }
}
