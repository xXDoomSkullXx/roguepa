﻿using UnityEngine;
using System.Collections;

public class KillEnemy : MonoBehaviour {

    public int enemyHealth;

    public GameObject deathEffect;

    public AudioSource audioEnemy;

    public int pointsOnDeath;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (enemyHealth <= 0)
        {
            Instantiate(deathEffect, transform.position, transform.rotation);
            ScoreCounter.AddPoints(pointsOnDeath);
            Destroy(gameObject);

        }

	}
    public void giveDamage(int damageToGive)
    {
        enemyHealth -= damageToGive;
        audioEnemy.Play();
    }
}
