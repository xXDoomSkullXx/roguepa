﻿using UnityEngine;
using System.Collections;

public class KillPLayer : MonoBehaviour {

    public int pointsToMinus;
    public LevelManager levelManager;
    public AudioSource playerAudio;

    private Animator animator;


	// Use this for initialization
	void Start () {
        levelManager = FindObjectOfType<LevelManager>();
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	

	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            levelManager.RespawnPlayer();
            ScoreCounter.MinusPoints(pointsToMinus);
            animator.SetTrigger("enemyAttack");
            playerAudio.Play();

        }

    }
}
