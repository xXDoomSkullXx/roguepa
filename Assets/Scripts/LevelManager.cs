﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject currentCheckPoint;
	public GameObject deathParticle;
	public GameObject respawnParticle;

	public float respawnDelay;

	private PlayerController player;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void RespawnPlayer()
	{
		StartCoroutine ("RespawnPlayerCo");
	}

	public IEnumerator RespawnPlayerCo() 
	{
        player.GetComponent<Rigidbody2D>().gravityScale = 0f;
        Instantiate (deathParticle, player.transform.position, player.transform.rotation);
		player.enabled = false;
		player.GetComponent<Renderer> ().enabled = false;
		yield return new WaitForSeconds (respawnDelay);
		player.transform.position = currentCheckPoint.transform.position;
        player.enabled = true;
        player.GetComponent<Rigidbody2D>().gravityScale = 5;
        player.GetComponent<Renderer> ().enabled = true;
		Instantiate (respawnParticle, currentCheckPoint.transform.position, currentCheckPoint.transform.rotation);
	}
    
}

